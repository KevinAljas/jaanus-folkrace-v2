#include <gyro.h>
#include <math.h>

#define WHO_AM_I 0x75
#define PWR_MGMT_1 0x6B
#define SMPLRT_DIV 0x19 // Data output rate, read documentation
#define GYRO_CONFIG 0x1B
#define ACCEL_CONFIG 0x1C

#define ACCEL_XOUT_H 0x3B
#define ACCEL_XOUT_L 0x3C
#define ACCEL_YOUT_H 0x3D
#define ACCEL_YOUT_L 0x3E
#define ACCEL_ZOUT_H 0x3F
#define ACCEL_ZOUT_L 0x40

#define GYRO_XOUT_H 0x43
#define GYRO_XOUT_L 0x44
#define GYRO_YOUT_H 0x45
#define GYRO_YOUT_L 0x46
#define GYRO_ZOUT_H 0x47
#define GYRO_ZOUT_L 0x48

#define INT_ENABLE 0x38

Gyroscope::Gyroscope(I2C_HandleTypeDef *hi2c) {
	this-> hi2c = hi2c;
	gyro_address = 208;

	euler_angle = {0, 0, 0};
}

void Gyroscope::BootGyro() {
	uint8_t data = 0;

	// Check if gyro is using default address
	HAL_I2C_Mem_Read(hi2c, gyro_address, WHO_AM_I, 1, &data, 1, HAL_TIMEOUT);
	if (data == 0) {
		gyro_address = ScanAddress();
	}

	// Activate clock
	data = 0x00;
	HAL_I2C_Mem_Write(hi2c, gyro_address, PWR_MGMT_1, 1, &data, 1, HAL_TIMEOUT);

	// Set sample rate
	data = 0x07;
	HAL_I2C_Mem_Write(hi2c, gyro_address, SMPLRT_DIV, 1, &data, 1, HAL_TIMEOUT);

	// Configure gyroscope and accelerometer
	data = 0x00;
	HAL_I2C_Mem_Write(hi2c, gyro_address, GYRO_CONFIG, 1, &data, 1, HAL_TIMEOUT);
	HAL_I2C_Mem_Write(hi2c, gyro_address, ACCEL_CONFIG, 1, &data, 1, HAL_TIMEOUT);

	data = 0x01;
	HAL_I2C_Mem_Write(hi2c, gyro_address, INT_ENABLE, 1, &data, 1, HAL_TIMEOUT);
}

Axis Gyroscope::ReadRawAccel() {
	Axis reading;

	// Read accelerometer data
	uint8_t data_in[6] = { 0 };
	HAL_I2C_Mem_Read(hi2c, gyro_address, ACCEL_XOUT_H, 1, data_in, 6, HAL_TIMEOUT);

	reading.x = (int16_t) (data_in[0] << 8 | data_in[1]);
	reading.y = (int16_t) (data_in[2] << 8 | data_in[3]);
	reading.z = (int16_t) (data_in[4] << 8 | data_in[5]);

	return reading;
}

Axis Gyroscope::ReadRawGyro() {
	Axis reading;

	uint8_t data_in[6] = { 0 };
	HAL_I2C_Mem_Read(hi2c, gyro_address, GYRO_XOUT_H, 1, data_in, 6, HAL_TIMEOUT);

	reading.x = (int16_t) (data_in[0] << 8 | data_in[1]);
	reading.y = (int16_t) (data_in[2] << 8 | data_in[3]);
	reading.z = (int16_t) (data_in[4] << 8 | data_in[5]);

	return reading;
}

/*
void Gyroscope::AddEulerAngle(uint16_t delta_time) {
	Axis reading = ReadRawGyro();

	if (abs(reading.x) > 1000) euler_angle.x += reading.x * delta_time * (180 / 3.14) / 1000000 / 131.0;
	if (abs(reading.y) > 1000) euler_angle.y += reading.y * delta_time * (180 / 3.14) / 1000000 / 131.0;
	if (abs(reading.z) > 1000) euler_angle.z += reading.z * delta_time * (180 / 3.14) / 1000000 / 131.0;
}
*/

void Gyroscope::OrientationWithComplementaryFilter(uint16_t delta_time) {
	Axis gyro_data = ReadRawGyro();
	gyro_data /= 131;

	Axis accl_data = ReadRawAccel();
	accl_data /= 16384;

	euler_angle = 0.98 * (euler_angle + gyro_data * delta_time) + 0.02 * accl_data;
	return;
}

/*
GyroData Gyroscope::RawToReadable(GyroData raw_data) {
	raw_data.accel_x /= 16384;
	raw_data.accel_y /= 16384;
	raw_data.accel_z /= 16384;

	raw_data.gyro_x /= 131;
	raw_data.gyro_y /= 131;
	raw_data.gyro_z /= 131;

	return raw_data;
}
*/

int Gyroscope::ScanAddress() {
	uint8_t address = 0;
	uint8_t response = 0;

	// Check all 1 byte addresses for response
	for (; response == 0 && address < 256; address++) {
		HAL_I2C_Mem_Read(hi2c, address, WHO_AM_I, 1, &response, 1, HAL_TIMEOUT);
	}

	return address - 1;
}
