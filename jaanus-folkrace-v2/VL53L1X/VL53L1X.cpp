#include <VL53L1X.h>

// Garbage horrible proprietary goblin API eww (also the header is broken which is why I included a source file)
#include <VL53L1X_api.h>
//#include "./Src/VL53L1X_api.c"

VL53L1X::VL53L1X(I2C_HandleTypeDef *hi2c, GPIO_TypeDef *xshut_port, uint16_t xshut_pin, GPIO_TypeDef *interrupt_port, uint16_t interrupt_pin) {
	this->hi2c = hi2c;

	this->xshut_port = xshut_port;
	this->xshut_pin = xshut_pin;

	this->interrupt_port = interrupt_port;
	this->interrupt_pin = interrupt_pin;
}

uint8_t VL53L1X::BootDevice(uint16_t new_address) {
	TurnOn();
	HAL_Delay(100);

	VL53L1X_SensorInit(address);

	HAL_Delay(50);

	VL53L1X_SetI2CAddress(address, new_address);
	address = new_address;

	return 0;
}

void VL53L1X::TurnOn() {
	HAL_GPIO_WritePin(xshut_port, xshut_pin, GPIO_PIN_SET);
}

void VL53L1X::TurnOff() {
	HAL_GPIO_WritePin(xshut_port, xshut_pin, GPIO_PIN_RESET);
}

void VL53L1X::StartRanging() {
	VL53L1X_StartRanging(address);
}

void VL53L1X::StopRanging() {
	VL53L1X_StopRanging(address);
}

void VL53L1X::SetNewAddress(uint16_t new_address) {
	VL53L1X_SetI2CAddress(address, new_address);
	address = new_address;
}


void VL53L1X::GetDistance() {
	uint16_t reading = 0;
	VL53L1X_GetDistance(address, &reading);
	VL53L1X_ClearInterrupt(address);
	distance = reading;
}
