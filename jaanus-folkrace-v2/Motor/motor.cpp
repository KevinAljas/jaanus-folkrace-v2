#include <math.h>

#include <logic.h>
#include <motor.h>
#include <debug.h>

#define SOFT_START_PERIOD 50

Motor::Motor(int16_t offset, TIM_HandleTypeDef *htim, uint32_t channel, GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin) {
	this->offset = offset;
	this->htim = htim;
	this-> channel = channel;
	this-> GPIOx = GPIOx;
	this-> GPIO_Pin = GPIO_Pin;
}

void Motor::SetSpeed(int16_t speed) {
	__HAL_TIM_SET_COMPARE(htim, channel, abs(speed));
	if (speed < 0) {
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(GPIOx, GPIO_Pin, GPIO_PIN_SET);
	}
}

void Motor::Stop() {
	SetSpeed(0);
}

void Motor::SoftDrive(uint16_t delta_t) {
	start_time += delta_t;

	if (start_time < SOFT_START_PERIOD) return;
	start_time = 0;

	uint16_t delta_speed = (target_speed - current_speed) / 2;
	current_speed += delta_speed;

	SetSpeed(current_speed);
}

void Motor::SetTargetSpeed(int16_t speed, uint16_t distance) {
	int set_speed = speed + offset;
	set_speed = CompensateForMomentum(set_speed, distance);

	if (speed == 0) set_speed = 0;
	else if (abs(set_speed) / set_speed != abs(speed) / speed) set_speed /= 2;

	if (set_speed > 255) set_speed = 255;
	else if (set_speed < -255) set_speed = -255;

	target_speed = set_speed;
}

int16_t Motor::CompensateForMomentum(int16_t speed, uint16_t distance) {
	if ( (distance > SLOW_THRESHOLD) || (distance == -1) ) return speed;
	//speed -= distance * SLOW_SCALAR + 180;
	speed -= abs(distance * SLOW_SCALAR - 180);
	return speed;
}

void DualDrive(uint16_t speed, bool direction, MotorPair motors){
	motors.motor_left.SetSpeed(speed);
	motors.motor_right.SetSpeed(speed);
}

void DualStop(MotorPair motors) {
	motors.motor_left.Stop();
	motors.motor_right.Stop();
}
