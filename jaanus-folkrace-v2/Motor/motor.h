#ifndef MOTOR_H_
#define MOTOR_H_

#include <tim.h>

class Motor {
public:
	// Motor "enable" pin
	TIM_HandleTypeDef *htim;
	uint32_t channel;
	int16_t offset;

	int16_t target_speed = 0;
	int16_t current_speed = 0;

	uint16_t start_time = 0;

	// Motor "phase" pin
	GPIO_TypeDef *GPIOx;
	uint16_t GPIO_Pin;

	Motor(int16_t offset, TIM_HandleTypeDef *htim, uint32_t channel, GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
	void SetSpeed(int16_t speed);
	void Stop();
	void SoftDrive(uint16_t delta_t);
	void SetTargetSpeed(int16_t speed, const uint16_t distance = 0);
	int16_t CompensateForMomentum(int16_t speed, uint16_t distance);
};

struct MotorPair {
	Motor motor_left;
	Motor motor_right;
};

void DualDrive(uint16_t speed, bool direction, MotorPair motors);
void DualStop(MotorPair motors);

#endif // MOTOR_H_
