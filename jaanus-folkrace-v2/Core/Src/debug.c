#include <debug.h>
#include <gpio.h>


#define CUSTOM_DEBUGGER 1
void DebugData(const char *fmessage, ...) {
#if CUSTOM_DEBUGGER == 1
	char str[200] = { 0 };
	va_list values;

	va_start(values, fmessage);
	uint8_t length = vsprintf(str, fmessage, values);

	HAL_UART_Transmit(&huart2, (uint8_t *) str, length, 100);
#endif
	return;
}

void ToggleBootLed() {
	HAL_GPIO_WritePin(BOOT_LED_GPIO_Port, BOOT_LED_Pin, GPIO_PIN_SET);
	return;
}


