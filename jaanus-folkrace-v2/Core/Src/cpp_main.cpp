#include <cpp_main.h>

#include <string.h>
#include <stdio.h>

#include <motor.h>
#include <logic.h>
#include <gyro.h>

#include <tim.h>
#include <gpio.h>
#include <usart.h>
#include <i2c.h>

#include <debug.h>

#include <VL53L1X.h> // Self-made adapter code for garbage proprietary library

// Global variables
Motor right_motor(0, &htim1, TIM_CHANNEL_4, LEFT_MOTOR_DIR_GPIO_Port, LEFT_MOTOR_DIR_Pin);
Motor left_motor(0, &htim1, TIM_CHANNEL_1, RIGHT_MOTOR_DIR_GPIO_Port, RIGHT_MOTOR_DIR_Pin);

Motor *primary_motor;
Motor *secondary_motor;

Gyroscope gyro(&hi2c1);

VL53L1X sensor_a(&hi2c1, SENSOR_A_XSHUT_GPIO_Port, SENSOR_A_XSHUT_Pin, SENSOR_A_INT_GPIO_Port, SENSOR_A_INT_Pin);
VL53L1X sensor_b(&hi2c1, SENSOR_B_XSHUT_GPIO_Port, SENSOR_B_XSHUT_Pin, SENSOR_B_INT_GPIO_Port, SENSOR_B_INT_Pin);
VL53L1X sensor_c(&hi2c1, SENSOR_C_XSHUT_GPIO_Port, SENSOR_C_XSHUT_Pin, SENSOR_C_INT_GPIO_Port, SENSOR_C_INT_Pin);

VL53L1X *primary_sensor;
VL53L1X *center_sensor = &sensor_b;
VL53L1X *secondary_sensor;

// Romani Kood - Roman on seksi kõva cool vana #coolvana

void cpp_main() {
	DebugData("Starting boot\n\r");

	HAL_TIM_Base_Start(left_motor.htim);
	HAL_TIM_PWM_Start(left_motor.htim, left_motor.channel);

	HAL_TIM_Base_Start(right_motor.htim);
	HAL_TIM_PWM_Start(right_motor.htim, right_motor.channel);

	HAL_TIM_Base_Start(&htim7); // Microsecond clock (overflows in 65ms)
	HAL_TIM_Base_Start(&htim2); // Millisecond clock for motor soft start (overflows in ~50 days lol)

	// Setup VL53L1X sensor addresses
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	HAL_NVIC_DisableIRQ(EXTI4_IRQn);
	HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);

	sensor_a.TurnOff();
	sensor_b.TurnOff();
	sensor_c.TurnOff();
	HAL_Delay(10);

	DebugData("Booting distance sensor: ");
	sensor_a.BootDevice(0x32);
	DebugData("A ");

	sensor_b.BootDevice(0x64);
	DebugData("B ");

	sensor_c.BootDevice(0x96);
	DebugData("C OK\n\r");

	sensor_a.StartRanging();
	sensor_b.StartRanging();
	sensor_c.StartRanging();

	HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

	// Set driving side
	if (HAL_GPIO_ReadPin(DIR_TOG_GPIO_Port, DIR_TOG_Pin)) { // Right hand side
		primary_sensor = &sensor_c;
		secondary_sensor = &sensor_a;

		primary_motor = &right_motor;
		secondary_motor = &left_motor;
	} else { // Left hand side
		primary_sensor = &sensor_a;
		secondary_sensor = &sensor_c;

		primary_motor = &left_motor;
		secondary_motor = &right_motor;
	}

	ToggleBootLed();

	// Small hack to compensate for unstable sensor booting
	bool button_state = HAL_GPIO_ReadPin(DIR_TOG_GPIO_Port, DIR_TOG_Pin);
	while(HAL_GPIO_ReadPin(DIR_TOG_GPIO_Port, DIR_TOG_Pin) == button_state);

	HAL_TIM_Base_Start_IT(&htim6); // Millisecond clock for motor soft start
	return;
}

void cpp_loop() {
	//primary_motor->SetTargetSpeed(180, center_sensor->distance);
	//secondary_motor->SetTargetSpeed(180, center_sensor->distance);

	//DebugData("Gigacunts: %d\n\r", center_sensor->distance);
	DebugData("L:%5d C:%5d R:%5d         %4d %4d\n\r", sensor_a.distance, sensor_b.distance, sensor_c.distance, left_motor.target_speed, right_motor.target_speed);
}

// Peripheral reading interrupts
extern "C" void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if (GPIO_Pin == center_sensor->interrupt_pin) {
		center_sensor->GetDistance(); // Check for obstacle through center sensor
		CrashAvoidance(primary_motor, secondary_motor, center_sensor);
		return;
	}

    if (GPIO_Pin == primary_sensor->interrupt_pin) {
    	primary_sensor->GetDistance();
    	goto set_target_speed;
    }

    if (GPIO_Pin == secondary_sensor->interrupt_pin) {
    	secondary_sensor->GetDistance();
    	goto set_target_speed;
	}

    set_target_speed:
	CruiseDrive(primary_sensor->distance, secondary_sensor->distance, primary_motor, secondary_motor);
	return;
}

// Slow start motor tick
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    if (htim == &htim6) {
    	primary_motor->SoftDrive(1);
    	secondary_motor->SoftDrive(1);
    }
}
