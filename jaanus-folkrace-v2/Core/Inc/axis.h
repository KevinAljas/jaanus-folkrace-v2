#ifndef AXIS_H_
#define AXIS_H_

struct Axis {
	double x;
	double y;
	double z;

	// Adding
	Axis& operator+(const Axis& other) {
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
		return *this;
	}

	Axis& operator+=(const Axis& other) {
		return *this + other;
	}

	// Subtracting
	Axis& operator-(const Axis& other) {
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
		return *this;
	}

	Axis& operator-=(const Axis& other) {
			return *this - other;
		}


	// Multiplication
	Axis& operator*(const Axis& other) {
		this->x *= other.x;
		this->y *= other.y;
		this->z *= other.z;
		return *this;
	}

	Axis& operator*=(const Axis& other) {
			return *this * other;
		}

	// Division
	Axis& operator/(const Axis& other) {
		this->x /= other.x;
		this->y /= other.y;
		this->z /= other.z;
		return *this;
	}

	Axis& operator/=(const Axis& other) {
			return *this / other;
		}

	// Scalar multiplication
	Axis& operator*(const double scalar) {
		this->x *= scalar;
		this->y *= scalar;
		this->z *= scalar;
		return *this;
	}

	Axis& operator*=(const double scalar) {
		return *this * scalar;
	}

	// Scalar division
	Axis& operator/(const double scalar) {
		this->x /= scalar;
		this->y /= scalar;
		this->z /= scalar;
		return *this;
	}

	Axis& operator/=(const double scalar) {
		return *this / scalar;
	}
};

// Commutative scalar multiplication
inline Axis operator*(double scalar, Axis &axis) {
	return axis * scalar;
}

#endif // AXIS_H_
