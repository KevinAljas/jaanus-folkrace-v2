#ifndef DEBUGGER_H_
#define DEBUGGER_H_

#include <stdarg.h>
#include <stdio.h>
#include <usart.h>

#ifdef __cplusplus
extern "C"
{
#endif

void DebugData(const char *fmessage, ...);
void ToggleBootLed();

#ifdef __cplusplus
}
#endif

#endif
