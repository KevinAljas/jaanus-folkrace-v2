#include <logic.h>

#include <motor.h>
#include <VL53L1X.h>
#include <math.h>

#include <debug.h>

void BasicDelay(unsigned long delay) {
	unsigned long time_start = __HAL_TIM_GET_COUNTER(&htim2);
	while(time_start + delay > __HAL_TIM_GET_COUNTER(&htim2));
}

void CruiseDrive(uint16_t primary_distance, uint16_t secondary_distance, Motor *primary_motor, Motor *secondary_motor) {
	//static int previous_error = 0;

	// Calculate difference in error
	//int primary_error = 2 * primary_distance / 3;
	//int secondary_error = 1 * primary_distance / 3;
	int32_t error = (int32_t) TARGET_DISTANCE - (int32_t) primary_distance;

	// Set motor target speeds
	//primary_motor->SetTargetSpeed((int16_t) AVG_SPEED + (POR * error) ); // - (DER * abs(error - previous_error)) );
	//secondary_motor->SetTargetSpeed((int16_t) AVG_SPEED - (POR * error) ); //+ (DER * abs(error - previous_error)) );

	//revious_error = error;
}

void CrashAvoidance(Motor *primary_motor, Motor *secondary_motor, VL53L1X *center_sensor) {
	if (center_sensor->distance > CRASH_THRESHOLD) return;
	// Crash course detected, this function takes full priority, disable all threads
	// Keep in mind HAL_Delay() requires needs Systick interrupt to work
	// Same with interrupt based sensor polling
	__disable_irq();

	// Full stop to lose all momentum
	primary_motor->SetSpeed(0);
	secondary_motor->SetSpeed(0);
	BasicDelay(RECOVERY_TIME);

	// Reorient away from obstacle
	primary_motor->SetSpeed(80);
	secondary_motor->SetSpeed(-80);
	while(center_sensor->distance < CRASH_THRESHOLD + 100) {
		center_sensor->GetDistance();
		BasicDelay(10);
	}

	__enable_irq();
}
