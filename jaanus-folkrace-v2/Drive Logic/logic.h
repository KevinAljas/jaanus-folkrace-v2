#ifndef LOGIC_H_
#define LOGIC_H_

#include <motor.h>
#include <VL53L1X.h>

// Cruise control variables
#define AVG_SPEED 180 // Average speed lol
#define TARGET_DISTANCE 200 // Distance from priority wall
#define POR(x) x / 10 // Cruise Drive PID controller - Porportional
#define DER 0 // Cruise Drive PID controller - Derivative

// Crash avoidance variables
#define SLOW_THRESHOLD 400 // Minimum difference before slowing average speed
#define SLOW_SCALAR 9 / 20 // Scalar to reduce average speed
#define CRASH_THRESHOLD 150 // Maximum distance before reacting to frontal obstacle
#define RECOVERY_TIME 300 // Time to lose momentum after fast break

void BasicDelay(unsigned long delay);

void CruiseDrive(uint16_t primary_distance, uint16_t secondary_distance, Motor *primary_motor, Motor *secondary_motor);
void CrashAvoidance(Motor *primary_motor, Motor *secondary_motor, VL53L1X *center_sensor);

#endif // LOGIC_H_
